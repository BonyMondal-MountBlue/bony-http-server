const http = require("http");

const fs = require("fs");

const { v4: uuidv4 } = require("uuid");

const PORT = 8000;

const server = http.createServer((req, res) => {
  try {
    /**
     * GET /html - It will return the following html content (but web browser doesn't return the code)
     */
    if (req.method === "GET" && req.url === "/json") {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(
        JSON.stringify({
          data: {
            slideshow: {
              author: "Yours Truly",
              date: "date of publication",
              slides: [
                {
                  title: "Wake up to WonderWidgets!",
                  type: "all"
                },
                {
                  items: [
                    "Why <em>WonderWidgets</em> are great",
                    "Who <em>buys</em> WonderWidgets"
                  ],
                  title: "Overview",
                  type: "all"
                }
              ],
              title: "Sample Slide Show"
            }
          }
        })
      );
    } else if (req.method === "GET" && req.url === "/html") {
      /**
       * GET /json - It will return the following json content
       */
      fs.readFile("./public/index.html", (err, data) => {
        if (err) {
          let err = { data: "HTML content not found" };
          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(JSON.stringify(err));
        } else {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.end(data);
        }
      });
    } else if (req.method === "GET" && req.url === "/uuid") {
      /**
       * GET /uuid - It is returning the uuid version 4 codes.
       */
      res.writeHead(200, { "Content-Type": "application/json" });
      let data = { uuid: uuidv4() };
      res.end(JSON.stringify(data));
    } else if (req.method === "GET" && req.url.match("/status")) {
      /**
       * GET /status/{status_code} : It will return the status code message with the status code.
       */
      let StatusCode = +req.url.slice(8);
      res.writeHead(200, { "Content-Type": "text/plain" });
      res.end(`${StatusCode} ${http.STATUS_CODES[StatusCode]}`);
    } else if (req.method === "GET" && req.url.match("/delay")) {
      /**
       * GET /delay/{delay_in_seconds} : It will create a delay for some time then return the success.
       */
      let delayTime = +req.url.slice(7);
      if (isNaN(delayTime) || delayTime < 1 || delayTime > 10) {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(http.STATUS_CODES[404]);
      } else {
        setTimeout(() => {
          res.writeHead(200, { "Content-Type": "text/plain" });
          res.end(`Delay time successfully with the delay of ${delayTime}`);
        }, delayTime * 1000);
      }
    } else {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(http.STATUS_CODES[400]);
    }
  } catch (err) {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end(http.STATUS_CODES[400]);
  }
});

server.listen(PORT, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Server started on port ${PORT}`);
  }
});
